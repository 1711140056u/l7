class CartItemsController < ApplicationController
  def create
    @cart_item = Cart_item.new(name: params[:product][:name], price: params[:product][:price])
    if @cart_item.save
      flash[:info] = '商品を登録完了しました。'
      redirect_to top_main_path
    end
  end
  
  def destroy
    cart_item = Cart_item.find(params[:id])
    cart_item.destroy
    redirect_to cart_items_path
  end
end
